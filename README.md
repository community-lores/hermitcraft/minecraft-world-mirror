# Minecraft World Storj DCS Mirror - Issue Tracker

This GitLab project is only used as an issue tracker for issues related to Andrei Jiroh's mirror of map downloads, mostly Hermitcraft and Empires SMP, on Storj DCS. The rest of this README is mostly documentation stuff.

While we manage the bucket on the EU satelite, files in Storj DCS are multi-region by default and stored in storage nodes managed by different parties.

## Documentation

Docs about the mirror is available at <https://ajhalili2006.gitbook.io/mc-world-map-mirrors/>.

## Donate

**PayPal**: <https://paypal.me/ajhalili2006?country.x=PH&locale.x=en_US> or [via its delicated page](https://www.paypal.com/donate/?hosted_button_id=6R3HU769HNUTE)
